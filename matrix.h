//
// Created by Alina Kidenko on 23/01/2021.
//

#ifndef PJC_SEMESTRAL_WORK_MATRIX_H
#define PJC_SEMESTRAL_WORK_MATRIX_H
#include <vector>

class matrix {
public:
    void setDim(int dim);
    std::vector<std::vector<double>> values;
    void print_matrix();
    matrix* create_identity_matrix(int d);
    matrix* make_inverse();
    matrix();

    ~matrix();

private:
    int dim; //dimension of square matrix
};


#endif //PJC_SEMESTRAL_WORK_MATRIX_H
