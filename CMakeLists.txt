cmake_minimum_required(VERSION 3.17)
project(pjc_semestral_work)

set(CMAKE_CXX_STANDARD 14)

add_executable(pjc_semestral_work main.cpp matrix.cpp matrix.h)