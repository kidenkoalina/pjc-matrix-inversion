//
// Created by Alina Kidenko on 23/01/2021.
//

#include "matrix.h"
#include <iostream>
#include <cmath>

void matrix::print_matrix() {

    for (int i = 0; i < this->values.size(); i++){
        for (int j = 0; j < this->values[i].size(); j++){
            std::cout << this->values[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void matrix::setDim(int dim) {
    matrix::dim = dim;
}

matrix* matrix::create_identity_matrix(int d) {
    auto m = new matrix();
    for (int i = 0; i < d; ++i) {
        std::vector<double> v;
        for (int j = 0; j < d; ++j) {
            int val = 0;
            if(i == j) val = 1;
            v.push_back(val);
        }
        m->values.push_back(v);
    }
    return m;
}

matrix::matrix(){}

matrix* matrix::make_inverse() {

    int D = this->dim; //D = dimension
    matrix* imx = create_identity_matrix(D); // imx = identity matrix

//----1.step----------------------------------------------------------------------------------------
    // In this "for" block we will get main matrix to upper triangle form
    // and we also perform the same operations on the identity matrix
    // ex. 1 3 3      1 3 4
    //     1 4 3 ---> 0 4 6
    //     1 3 4      0 0 1
    for (int r = 0; r < D; r++){

        // we are looking for the pivot row, the biggest first value of the row
        int max = r;
        for (int i = r + 1; i < D; i++)
            if (std::abs(this->values[i][r]) > std::abs(this->values[max][r]))
                max = i;

        // after we found the pivot row, we swap this row in the main matrix
        if(max != r){
            std::vector<double> temp = this->values[r];
            this->values[r] = this->values[max];
            this->values[max] = temp;

            //do the same swap in our identity matrix
            std::vector<double> t = imx->values[r];
            imx->values[r] = imx->values[max];
            imx->values[max] = t;
        }

        //making zeros on the right places
        for (int i = r + 1; i < D; i++){
            double factor = this->values[i][r] / this->values[r][r];
            for (int j = 0; j < D; j++){
                this->values[i][j] -= factor * this->values[r][j];
                imx->values[i][j] -= factor * imx->values[r][j]; //making the same changes in the identity matrix
            }
        }
//        std::cout << "---------making down 0----------" << std::endl;
//        std::cout << "this " << std::endl;
//        this->print_matrix();
//        std::cout << "imx " << std::endl;
//        imx->print_matrix();
    }

//--------------------------------------------------------------------------------------------
    //Now we want to know if there is a zero row. If there is, then matrix doesn't have an inversion.
    for (int i = 0; i < D; ++i) {
        int z = 0;
        for (int j = 0; j < D; ++j) {
            if(this->values[i][j] == 0) z++;
        }
        if(z == D) {
            delete imx;
            throw "Matrix is singular and therefore doesn't have an inversion.";
        }
    }

//----2.step----------------------------------------------------------------------------------------
    //Now we will be making 1's on the diagonal
    // ex. 2 3 4      1 3 4
    //     0 4 6 ---> 0 1 6
    //     0 0 5      0 0 1
    for (int i = 0; i < D; ++i) {
        double factor = this->values[i][i];
        for (int j = 0; j < D; ++j) { //j is going through a row
            this->values[i][j] /= factor;
            imx->values[i][j] /= factor; //making the same changes in the identity matrix
        }
    }

//    std::cout << "--------- made 1's ----------" << std::endl;
//    std::cout << "this " << std::endl;
//    this->print_matrix();
//    std::cout << "imx " << std::endl;
//    imx->print_matrix();

//----3.step----------------------------------------------------------------------------------------
    //Now we have the upper triangle matrix with 1's on the diagonal
    //Now I want to make 0's in a upper part of matrix to get identity matrix
    // ex. 1 3 4      1 0 0
    //     0 1 6 ---> 0 1 0
    //     0 0 1      0 0 1
    for (int j = D - 1; j > 0; --j) {
        for (int i = j; i > 0; --i) {
            double factor = this->values[i-1][j];
            for (int k = 0; k < D; ++k) {
                this->values[i - 1][k] -= this->values[j][k] * factor;
                imx->values[i - 1][k] -= imx->values[j][k] * factor; //making the same changes in the identity matrix
            }
        }

//        std::cout << "--------- making up 0's----------" << std::endl;
//        std::cout << "this " << std::endl;
//        this->print_matrix();
//        std::cout << "imx " << std::endl;
//        imx->print_matrix();

    }
    return imx;
}

matrix::~matrix() {}
