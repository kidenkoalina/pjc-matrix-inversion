# pjc-semestral-work

Semestrální projekt z předmětu PJC (C++).

## Zadání
Aplikace na vstupu dostane čtvercovou matici a na výstup vypíše její inverzi, případně zprávu o tom, že matice nemá inverzi. K implementaci se používá Gaussova eliminace.

## Vstup aplikace
Aplikace přijímá pouze čtvercové matice. V případě, že zadaných uživatelem prvků je méně, program bude čekat na zbylé prvky. V případě, že prvků je víc, aplikace zpracuje prvních N, které jsou potřeba k matici rozměrem D*D. 

## Výstup aplikace
Výstupem aplikace je inverzní matice v případě, že taková existuje.
Nebo hláška "Matrix is singular and therefore doesn't have an inversion." v případě, kdy matice nemá inverzi.

## Vzorová vstupní data
Spusťte program a počkejte na výzvu k zadání parametrů:

   1. parametr je rozměr ("počet řádků") čtvercové matice.
   2. parametr je řada čísel, z nichž se vaše matice skládá
   
   Například:
   1. parametr: 2
   2. parametr: 1 2 3 4
   
   To znamená, že vaše matice je
             
             / 1 2 \
             \ 3 4 /
             
  Další příklady vstupních dat:
                
      2
      1 2 1 2
      
      3
      1 0 4 1 1 6 -3 0 -10
      
      3
      1 3 3 1 4 3 1 3 4 
      
      3
      1 2 3 1 3 2 0 0 1

      4
      7 6 -12 -15 -2 -11 -1 9 10 6 -4 10 13 15 9 15
      

## Implementace
Výpočet se provádí ve třech krocích.
1. Převedení matice do horního trojúhelníkového tvaru. (Gaussova eliminace)
2. Vynásobení každého řádku takovou hodnotou, která vytvoří jedničku na diagonále.
3. Docílení se nul v matici nad diagonálou. Což se udělá jednoduše kvůli jedničkám na diagonále.

Tyhle tři kroky se provádějí na hlavní matici, kterou zadal uživatel. 
Vedle toho se provádějí stejné operace na jednotkové matici. Po provedení všech kroků místo jednotkové matice se nám objeví inverzní matice.

## Měření
I přestože jsem neimplementovala vícevlaknovou aplikaci, stejně jsem udělala jednoduché měření času vůči různým modům: Debug mode a Release mode.

Pro matici 3x3 s hodnotami 1 2 3 1 3 2 0 0 1 jsem naměřila následující časy. 
Je vidět, že Release mode skutečně optimalizuje běh programu.

Release mode:
- Needed 29778 ms to finish.
- Needed 29898 ms to finish.
- Needed 29241 ms to finish.
- Needed 28613 ms to finish.

Debug mode:
- Needed 37641 ms to finish.
- Needed 38558 ms to finish.
- Needed 38858 ms to finish.
- Needed 38562 ms to finish.
