#include <iostream>
#include <string>
#include <chrono>
#include <algorithm>
#include "matrix.h"

bool runProgram(){

//-------------- Reading input --------------
    auto mx = new matrix();
    std::cout << "Enter a square matrix dimension: ";
    int dim;
    std::cin >> dim;
    mx->setDim(dim);

    std::cout << "Enter values of the matrix: ";
    //reading values from terminal
    for (int i = 0; i < dim; ++i) {
        std::vector<double> v;
        for (int j = 0; j < dim; ++j) {
            int val;
            std::cin >> val;
            v.push_back(val);
        }
        mx->values.push_back(v);
    }

    std::cout << "Your matrix: " << std::endl;
    mx->print_matrix();

//-------------- Computation --------------
    auto start = std::chrono::high_resolution_clock::now();
    try {
        matrix* inverse = mx->make_inverse();
        std::cout << "Inverse matrix: " << std::endl;
        inverse->print_matrix();
        delete inverse;
    } catch (const char* msg) {
        std::cerr << msg << std::endl;
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Needed " << (end - start).count() << " ms to finish.\n";

    delete mx;

    return 0;
}
bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

int main(int argc, char* argv[]) {

    // --help
    if (std::any_of(argv, argv+argc, is_help)) {
        printf("Uses of program: \n"
               "    This program will calculate for you the inversion for the square matrix.\n"
               "\n"
               "    Run program and wait for prompt to enter the parameters:  \n"
               "        First parameter is a dimension of square matrix. \n"
               "        Second parameter is a set of numbers your matrix is consist from. \n"
               "        For example:  \n"
               "            1.parameter: 2 \n"
               "            2.parameter: 1 2 3 4 \n"
               "            That means that yout matrix is 1 2 \n"
               "                                           3 4 \n"
               "\n"
               "    And after you hit the enter button, you will get the answer:  \n"
               "       Inverse matrix: \n"
               "       -2 1 \n"
               "       1.5 -0.5 \n"

               );
        return 0;
    } else if (argc > 1) {
        printf("Unknown parameter. You can try --help parameter. \n");
        return 0;
    }

    //program to calculate the inverse of matrix
    return runProgram();
}
